class Recommender_AS:
    import pandas as pd
    import numpy as np
    import matplotlib.pyplot as plt
    
    
    ################################################################################
    def createR(Y):
        R = Y!=0
        R = np.asarray(R, dtype = 'int')
        return R
    
    ################################################################################
    def normalise_ratings(Y, R):
        num_books, num_users = Y.shape
        Y_mean = np.zeros(shape=(num_books, 1))
        Y_norm = np.zeros(shape=(Y.shape))

        for i in range(num_books):
            idx = np.where(R[i, :] == 1)
            Y_mean[i] = Y[i, idx].mean()
            Y_norm[i, idx] = Y[i, idx] - Y_mean[i]

        return Y_norm, Y_mean

    ################################################################################
    def param_initialize(num_books, num_features, num_users):
        epsilon = 0.1
        X = np.random.rand(num_books,num_features)*2*epsilon - epsilon
        Theta = np.random.rand(num_users, num_features)*2*epsilon - epsilon
        params = np.append(X.flatten(), Theta.flatten())
        return params
    
    ################################################################################
    def param_initialize_ALS(num_books, num_features, num_users, Y_mean):
        epsilon = 0.1
        X = np.random.rand(num_books,num_features)*2*epsilon - epsilon
        print(X[:,0].shape)
        X[:,0]=Y_mean.T
        Theta = np.random.rand(num_users, num_features)*2*epsilon - epsilon
        params = np.append(X.flatten(), Theta.flatten())
        return params
    
    ################################################################################
    def param_reshape(params, num_books, num_features, num_users):
        X = params[:num_books*num_features].reshape(num_books, num_features)
        Theta = params[num_books*num_features:].reshape(num_users, num_features)
        return X, Theta
    
    ################################################################################
    def cost_function(params, Y, R, num_users, num_features, num_books, lamda):
        # Reshape params back to X and Theta
        X, Theta = param_reshape(params, num_books, num_features, num_users)
        
        # Initialise cost and gradients
        J = 0
        J = (1/2)*((((np.matmul(X, Theta.T)*R)-Y)**2).sum()).sum()
        J_reg = (lamda/2)*((Theta**2).sum()).sum() + (lamda/2)*((X**2).sum()).sum()
        
        # Add regularisation term to cost
        J = J + J_reg
        return J
    
    ################################################################################
    def grad_function(params, Y, R, num_users, num_features, num_books, lamda):
        # Reshape params back to X and Theta
        X, Theta = param_reshape(params, num_books, num_features, num_users)

        # Initialise cost and gradients
        X_grad = np.zeros(shape=X.shape)
        Theta_grad = np.zeros(shape=Theta.shape)

        X_grad = np.matmul(((np.matmul(X, Theta.T)*R)-Y), Theta) + (lamda*X)
        Theta_grad = np.matmul(((np.matmul(X, Theta.T)*R)-Y).T, X) + (lamda*Theta)

        # Flatten X_grad and Theta_grad and combine to a 1D vector
        grad = np.append(X_grad.flatten(), Theta_grad.flatten())
        return grad
    
    ################################################################################
    def SGD(params, *args, iters, alpha):
        # Initialise params
        params = param_initialize(num_books, num_features, num_users)
        J_hist=np.zeros(shape=(1,1))

        # Start iterations
        for i in range(iters):
            # Calculate costs
            J = cost_function(params, *args)
            J_hist = np.append(J_hist, J)

            # Calculate gradients
            grad = grad_function(params, *args)

            # Apply update rule
            X_params = params[:num_books*num_features] + alpha*grad[0:num_books*num_features]
            Theta_params = params[num_books*num_features:] + alpha*grad[num_books*num_features:]

            # Flatten parameters
            params = np.append(X_params.flatten(), Theta_params.flatten())
            if i%100 == 0:
                print(i, J)
            if np.abs(J-J_hist[-2]) <= 1e-2:
                print('Convergence Criteria of J <= 1e-2 met. Exiting.')
                break

        print(str(i) + ' Iterations completed.')
        return params, J_hist
    
    ################################################################################
    def miniBatchSGD(params, *args, iters, alpha):
        # Initialise params
        params = param_initialize(num_books, num_features, num_users)
        R_temp=R.copy()
        J_hist=np.zeros(shape=(1,1))

        # Start iterations
        for i in range(iters):
            
            # Identify where in Y matrix there are known ratings.
            rows_filled, columns_filled = np.where(R_temp==1)

            # If all known ratings have been used up, reset R_temp back to R and continue learning algorithm.
            if len(rows_filled)==0:
                R_temp=R.copy()
                rows_filled, columns_filled = np.where(R_temp==1)

            # Randomly select a batch of 100 from the known rating locations in R. 
            # Set those locations in R_temp to 0 so they are not selected again until R_temp is reset.
            random_selection = np.random.randint(0, high=len(rows_filled), size=100)
            R_temp[rows_filled[random_selection], columns_filled[random_selection]] = 0

            # Calculate costs and append cost history
            J = cost_function(params, *args)
            J_hist = np.append(J_hist, J)
            
            # Extract the relevant parameters from the selected batch and format them correctly for gradient calculations.
            X, Theta = param_reshape(params, num_books, num_features, num_users)
            X_batch = X[rows_filled[random_selection], :]
            Theta_batch = Theta[columns_filled[random_selection], :]
            Y_batch = Y_norm[rows_filled[random_selection], columns_filled[random_selection]]
            R_batch = R[rows_filled[random_selection], columns_filled[random_selection]]
            params_batch = np.append(X_batch.flatten(), Theta_batch.flatten())

            # Calculate gradients
            args_batch = (Y_batch, R_batch, Theta_batch.shape[0], num_features, X_batch.shape[0], 0)
            grad = grad_function(params_batch, *args_batch)

            # Apply update rule to batch
            X_params = params_batch[0:X_batch.shape[0]*num_features] + alpha*grad[0:X_batch.shape[0]*num_features]
            Theta_params = params_batch[X_batch.shape[0]*num_features:] + alpha*grad[X_batch.shape[0]*num_features:]

            # Reshape parameters and update larger matrix
            params_temp = np.append(X_params, Theta_params)
            X_temp, Theta_temp = param_reshape(params_temp, X_batch.shape[0], num_features, Theta_batch.shape[0])
            
            # Input the updated batch back into the larger X and Theta matrices.
            X[rows_filled[random_selection], :] = X_temp
            Theta[columns_filled[random_selection], :] = Theta_temp
            params =np.append(X.flatten(), Theta.flatten())
            
            # Print the cost every 100th iteration and break the loop if the residual <= Convergence Criterion. 
            if i%100 == 0:
                print(i, J)
            if np.abs(J-J_hist[-2]) <= 1e-2:
                print('Convergence Criteria of J <= 1e-2 met. Exiting.')
                break

        print(str(i) + ' Iterations completed.')
        return params, J_hist 
    
    ################################################################################
    def ALS(params, *args, iters, alpha, Y_mean):

        # Initialise params
        params = param_initialize_ALS(num_books, num_features, num_users, Y_mean)
        #params = param_initialize(num_books, num_features, num_users)
        inner_iters = 1000
        J_hist=np.zeros(shape=(1,1))

        # Calculate costs
        J = cost_function(params, *args)
        J_hist = np.append(J_hist, J)

        # Set fix X flag
        fix_x=1
        
        # Start iterations
        # Outer Loop for switching between X and Theta matrices.
        for j in range(iters):
            
            if fix_x==1:
                print('Fix X matrix and optimise Theta matrix...')
            else:
                print('Fix Theta matrix and optimise X matrix...')
            
            # Start inner loop for gradient descent optimisation.
            for i in range(inner_iters):

                # Calculate gradients
                grad = grad_function(params, *args)

                # Apply update rule
                if fix_x==0:
                    X_params = params[:num_books*num_features] + alpha*grad[0:num_books*num_features]
                    Theta_params = params[num_books*num_features:]
                else:
                    X_params = params[:num_books*num_features]
                    Theta_params = params[num_books*num_features:] + alpha*grad[num_books*num_features:]

                # Flatten parameters
                params = np.append(X_params.flatten(), Theta_params.flatten())
                fix_x = not fix_x

                 # Calculate costs
                J = cost_function(params, *args)
                J_hist = np.append(J_hist, J)
                
                # Print cost every 100th iteration and set convergence criteria.
                if i%100 == 0:
                    print(str(i) + ' Inner loop iterations', J)
                if np.abs(J-J_hist[-2]) <= 1e-3:
                    print('Convergence Criteria of J <= 1e-3 met. Exiting innerloop.')
                    break

            print(str(i) + ' Inner Iterations completed.')

            if j%1 == 0:
                print(str(j) + ' Outer loop iterations', J)
            if np.abs(J-J_hist[-2]) <= 1e-3:
                print('Convergence Criteria of J <= 1e-3 met. Exiting outerloop.')
                break

        print(str(j) + ' Outer Iterations completed.')
        return params, J_hist 
    
    ################################################################################